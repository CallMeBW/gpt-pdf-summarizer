# summarize.py

This is a Python script that uses the OpenAI GPT-3.5 Turbo model to summarize chunks of a book about research methodology from a PDF file. The summarized chunks are written in markdown format and saved in a separate file.

## Prerequisites

- Python 3.x
- PyPDF2 library
- OpenAI Python library

Install them with `pip install -r requirements.txt`

## Usage

Run the script with the following command:

```shell
python summarize.py <pdf_path> <api_token>
```

- `<pdf_path>`: The path to the PDF file you want to summarize.
- `<api_token>`: Your OpenAI API token.

## How it works

1. The script takes the PDF file path and API token as command-line arguments.
2. It defines a constant `OUTPUT_PATH` for the output summary file.
3. It sets up the initial query for the GPT-3.5 Turbo model in markdown format.
4. The `pdf_to_string` function reads the PDF file and extracts the text from specified pages.
5. The `query_gpt` function sends the query to the GPT-3.5 Turbo model and retrieves the response.
6. The `main` function:
   - Removes the existing summary file, if present.
   - Initializes the starting page and the number of pages to summarize.
   - Iterates through chunks of pages until reaching the end of the PDF:
     - Reads the text from the PDF chunk using `pdf_to_string`.
     - Sends the query and the chunk text to GPT-3.5 Turbo using `query_gpt`.
     - Appends the summarized chunk with markdown formatting to the output file.
     - Updates the page range for the next iteration.
     - Sleeps for 21 seconds between iterations to comply with OpenAI usage limits.
7. Finally, the script executes the `main` function if run as the main script.

Make sure you have the necessary dependencies installed and replace `<pdf_path>` with the actual path to your PDF file and `<api_token>` with your OpenAI API token to use this script successfully.