import os
import time
import openai
import sys
import PyPDF2

try:
    PDF_PATH = sys.argv[1]
    TOKEN = sys.argv[2]
except Exception:
    print('Usage: summarize.py <pdf_path> <api_token>')
    exit()

openai.api_key = TOKEN
OUTPUT_PATH = 'summary.md'
QUERY= f"Below is a chunk of a book about research methodology. Please summarize this chunk in"\
       f" markdown. Include many details, make use of bullet lists, etc.\n"

def get_pdf_len(file_path) -> int:
    with open(file_path, 'rb') as file:
        pdf_reader = PyPDF2.PdfReader(file)
        return len(pdf_reader.pages)

def pdf_to_string(file_path, from_page: int, last_page: int) -> str:
    with open(file_path, 'rb') as file:
        pdf_reader = PyPDF2.PdfReader(file)
        text = ""
        for page_num in range(from_page, last_page + 1):
            page = pdf_reader.pages[page_num]
            text += page.extract_text()
        return text

def query_gpt(query: str) -> str:
    chat_completion = openai.ChatCompletion.create(model="gpt-3.5-turbo", 
                                                   messages=[{"role": "user", "content": query}])
    return chat_completion.choices[0].message.content

def main():
    try:
        os.remove(OUTPUT_PATH)
    except FileNotFoundError:
        pass
    page_start = 1
    pages = 5
    while (page_start + page_start) < get_pdf_len(PDF_PATH):
      print(page_start)
      pdf_text = pdf_to_string(PDF_PATH, page_start,page_start + pages)
      summary = query_gpt(QUERY + pdf_text)
      with open(OUTPUT_PATH, 'a') as f:
          f.write(f'\n\n# Pages {page_start} - {page_start + pages}\n')
          f.write(summary)
      page_start += pages + 1
      time.sleep(21) # rate limit of 3 queries per minute


if __name__ == "__main__":
    main()